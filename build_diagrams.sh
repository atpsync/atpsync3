#!/usr/bin/env bash

cd diagrams
rm -rf build
mkdir -p build
find . -type f -name '*.d2' -exec d2 --theme=0 --dark-theme=200 {} build/{}.svg \;
