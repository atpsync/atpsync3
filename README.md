# unisync

## (Temporary notes)

My laptop broke so I'm waiting for new one to arrive :)

I have my new laptop and I chose this project as my final school project for maturita so I'll have much more time to spend on this than I did until now ;)

For exploring the source code check out [this](https://gitlab.com/atpsync/atpsync3/-/tree/f6cda88fdb9c5122ad5c85b0e8170627ca1f3267/) revision as it's the last one to make sense for now.

When testing, the Czech Republic needed ~2GB RAM (that's before stripping unneeded tags) so if we assume linear growth that's ~23GiB for the whole USA. I expect required RAM to be much lower after optimizing for RAM usage. For now the school server this will probably run on has 70 GB RAM anyway and in worst-case scenario my Oracle instance has 24GB RAM.

## About
![project diagram](diagrams/build/project.d2.svg)

## License
Icons in the `diagrams/icons` folder are licensed differently.
