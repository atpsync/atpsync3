{
  inputs = {
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "nixpkgs/nixos-unstable";
  };

  outputs = { self, fenix, flake-utils, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        fenixPkgs = (fenix.packages.${system}.stable);
        rustPlatform = (pkgs.makeRustPlatform {
          inherit (fenixPkgs) cargo rustc;
        });
      in
      {
        defaultPackage = rustPlatform.buildRustPackage {
          pname = "osm-unisync";
          version = "0.0.0";
          src = ./.;
          cargoHash = "";
          SQLX_OFFLINE = "true";
          nativeBuildInputs = with pkgs; [
            wget
          ];
        };

        devShell = pkgs.mkShell
          {
            buildInputs = with pkgs; [
              d2
              openssl.dev
              pkg-config
              sqlx-cli
              fenixPkgs.toolchain
              wget
            ];
            SQLX_OFFLINE = "true";
          };
      });
}
