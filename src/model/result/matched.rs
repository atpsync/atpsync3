use crate::model::{existing_poi::ExistingPoi, import_poi::ImportPoi};

#[derive(Debug, Clone)]
pub struct ResultMatched {
    pub import_poi: ImportPoi,
    pub existing_poi: ExistingPoi,
}
