#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum PoiType {
    Node,
    Way,
    Relation,
}
