use std::collections::HashMap;
use std::hash::Hash;

use super::poi_type::PoiType;

#[derive(Debug, Clone)]
pub struct ExistingPoi {
    pub r#type: PoiType,
    pub id: i64,
    pub lat: f64,
    pub lon: f64,
    pub tags: HashMap<String, String>,
}

impl Eq for ExistingPoi {}

impl PartialEq for ExistingPoi {
    fn eq(&self, other: &Self) -> bool {
        self.r#type == other.r#type && self.id == other.id
    }
}

impl Hash for ExistingPoi {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.r#type.hash(state);
        self.id.hash(state);
    }
}
