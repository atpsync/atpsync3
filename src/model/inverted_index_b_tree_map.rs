use std::collections::BTreeMap;

#[derive(Default)]
pub struct InvertedIndexBTreeMap<K: Ord, V> {
    inner: BTreeMap<K, Vec<V>>,
}

impl<K: Ord, V> InvertedIndexBTreeMap<K, V> {
    pub fn push(&mut self, key: K, value: V) {
        self.inner.entry(key).or_default().push(value);
    }

    pub fn contains_key(&self, key: &K) -> bool {
        self.inner.contains_key(key)
    }

    pub fn get_slice(&self, key: &K) -> Option<&[V]> {
        self.inner.get(key).map(|v| &v[..])
    }
}
