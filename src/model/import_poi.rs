use std::collections::HashMap;
use std::hash::Hash;

#[derive(Debug, Clone)]
pub struct ImportPoi {
    pub id: String,
    pub lat: f64,
    pub lon: f64,
    pub tags: HashMap<String, String>,
    pub metadata: HashMap<String, String>,
}

impl Eq for ImportPoi {}

impl PartialEq for ImportPoi {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Hash for ImportPoi {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }
}
