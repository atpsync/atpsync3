use axum::Router;

use super::AppState;

pub fn get() -> Router<AppState> {
    Router::new()
}
