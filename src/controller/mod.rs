use axum::Router;

mod matcher;
mod results;
mod scee;
mod webui;

#[derive(Debug, Clone)]
struct AppState {}

pub async fn run() {
    let state = AppState {};

    let app = Router::new()
        .nest("/api/1/matcher", matcher::get())
        .nest("/api/1/results", results::get())
        .nest("/api/1/scee", scee::get())
        .nest("/api/1/webui", webui::get())
        .with_state(state);

    let listener = tokio::net::TcpListener::bind("0.0.0.0:3000").await.unwrap();
    axum::serve(listener, app).await.unwrap();
}
