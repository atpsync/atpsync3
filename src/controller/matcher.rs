use axum::{routing::post, Router};

use crate::service;

use super::AppState;

pub fn get() -> Router<AppState> {
    Router::new().route("/match", post(r#match))
}

async fn r#match() {
    service::matcher::run().await;
}
