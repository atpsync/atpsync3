mod config;
mod controller;
mod model;
mod module;
mod service;
mod utils;

use clap::Parser;
use config::Config;

#[tokio::main]
async fn main() {
    env_logger::Builder::new()
        .filter(None, log::LevelFilter::Info)
        .filter(Some("osm_unisync"), log::LevelFilter::Debug)
        .init();

    let config = Config::parse();

    match config.command {
        config::Command::Run {} => controller::run().await,
    }
}
