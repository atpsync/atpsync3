use std::collections::HashSet;

use crate::model::import_poi::ImportPoi;

pub mod alltheplaces;

pub trait Module {
    fn get_name(&self) -> &str;
    fn get_regions(&self) -> HashSet<String>;
    fn get_import_pois(&self) -> impl Iterator<Item = ImportPoi>;
}
