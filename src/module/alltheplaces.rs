use std::{collections::HashSet, fs::File, iter::FilterMap};

use crate::model::import_poi::ImportPoi;

use super::Module;

pub struct AllThePlaces {}

impl Module for AllThePlaces {
    fn get_name(&self) -> &str {
        "AllThePlaces"
    }

    fn get_regions(&self) -> HashSet<String> {
        let mut regions = HashSet::new();
        // regions.insert("europe/austria".to_owned());
        // regions.insert("europe/belgium".to_owned());
        regions.insert("europe/czech_republic".to_owned());
        // regions.insert("europe/finland".to_owned());
        // regions.insert("europe/france".to_owned());
        // regions.insert("europe/germany".to_owned());
        // regions.insert("europe/gibraltar".to_owned());
        // regions.insert("europe/guernesey".to_owned());
        // regions.insert("europe/ireland".to_owned());
        // regions.insert("europe/italy".to_owned());
        // regions.insert("europe/jersey".to_owned());
        // regions.insert("europe/luxembourg".to_owned());
        // regions.insert("europe/monaco".to_owned());
        // regions.insert("europe/netherlands".to_owned());
        // regions.insert("europe/norway".to_owned());
        // regions.insert("europe/poland".to_owned());
        // regions.insert("europe/portugal".to_owned());
        // regions.insert("europe/san_marino".to_owned());
        // regions.insert("europe/slovakia".to_owned());
        // regions.insert("europe/spain".to_owned());
        // regions.insert("europe/sweden".to_owned());
        // regions.insert("europe/switzerland".to_owned());
        // regions.insert("europe/turkey".to_owned());
        // regions.insert("europe/ukraine".to_owned());
        // regions.insert("europe/united_kingdom".to_owned());
        // regions.insert("europe/vatican_city".to_owned());
        regions
    }

    fn get_import_pois(&self) -> impl Iterator<Item = ImportPoi> {
        import("/home/vfosnar/osm-data/output.zip")
    }
}

use std::{
    collections::HashMap,
    fs,
    io::{Read, Write},
    path::Path,
};

use geojson::{FeatureCollection, GeoJson};
use log::{debug, warn};
use std::io::stdout;
use zip::read::ZipFile;

const SCRAPERS: &[&str; 100] = &[
    "adidas",
    "dm",
    "teta_cz",
    "tiffany",
    "esprit",
    "burger_king_cz",
    "newyorker",
    "sixt",
    "pandora",
    "foot_locker",
    "claires",
    "ich_tanke_strom",
    "accor",
    "travelex",
    "botteg_aveneta",
    "boconcept",
    "calliope",
    "billa",
    "pricewaterhousecoopers",
    "calvin_klein",
    "arb_4x4_accessories",
    "speed_queen",
    "gbfs",
    "ecco",
    "hard_rock",
    "longchamp_eu",
    "hugo_boss",
    "dr_max",
    "ernst_young",
    "church_of_england_gb",
    "pizza_hut_amrest",
    "kfc_amrest",
    "alstom",
    "golds_gym",
    "united_colors_of_benetton",
    "burger_king",
    "prada",
    "leonidas",
    "asics_eu",
    "moneygram",
    "mcdonalds_cz",
    "skechers",
    "jysk",
    "national",
    "lhw",
    "little_free_library",
    "bmw_group",
    "kik",
    "ford",
    "levis",
    "peugeot_se_cz_es",
    "coop_cz",
    "scania",
    "mercedes_benz_group",
    "avis",
    "enterprise",
    "crocs_eu",
    "greenway",
    "starbucks_eu",
    "a3_sport_cz",
    "total_energies",
    "audi",
    "deichmann",
    "tedi",
    "marriott_hotels",
    "man_truck_and_bus",
    "wyndham",
    "c_and_a",
    "metro_cash_and_carry",
    "shell",
    "timberland",
    "reebok",
    "omv",
    "petit_bateau",
    "montblanc",
    "renault",
    "hm",
    "deloitte",
    "coffeeshop_company",
    "ihg_hotels",
    "marionnaud",
    "emobilita_brno_cz",
    "google_offices",
    "nissan_cz",
    "pepco",
    "decathlon_cz",
    "penny",
    "obi_eu",
    "primark_cz",
    "ikea",
    "action",
    "imo",
    "the_north_face_eu",
    "ccc",
    "subway",
    "alamo",
    "kpmg",
    "nike",
    "hermes",
    "rsg_group",
];

const INVALID_SCRAPERS: [&str; 41] = [
    "santander_de", // https://github.com/alltheplaces/alltheplaces/issues/9116
    // no top level tags
    "our_airports",
    "sok_tr",
    "bonita",
    "mobilelink",
    "mini_be",
    "recipe_unlimited",
    "ok_foods",
    "my_documents_ru",
    "evgo",
    "an_post_ie",
    "cadillac",
    "chase_us",
    "bash_za",
    "my_dentist_gb",
    "mercator_si",
    "whole_foods",
    "tops",
    "orlen",
    "conad_it",
    "quality_dairy_us",
    "suzuki_marine_au",
    "blyzenko_ua",
    "seat",
    "skoda",
    "yoshinoya_us",
    "migros_tr",
    "politix_au",
    "maserati",
    "coop_no",
    "magasin_vert_fr",
    "systeme_u",
    "upim",
    "seat_de",
    "a101_tr",
    "south_carolina",
    "general_logistics_systems_de",
    "gov_bio123_de",
    "giant_my",
    "house_au",
    "vkusvill_ru",
];

fn import(archive_path: impl AsRef<Path>) -> impl Iterator<Item = ImportPoi> {
    let file = fs::File::open(archive_path).unwrap();
    let mut archive = zip::ZipArchive::new(file).unwrap();

    (0..archive.len())
        .filter_map(move |archive_i| {
            let mut file = archive.by_index(archive_i).unwrap();
            match parse_zipfile(&mut file) {
                Some(content) => Some(insert_geojson(&content)),
                None => None,
            }
        })
        .flatten()

    // let mut import_pois = HashSet::new();

    // let file = fs::File::open(archive_path).unwrap();
    // let mut archive = zip::ZipArchive::new(file).unwrap();
    // for i in 0..archive.len() {
    //     let mut file = archive.by_index(i).unwrap();
    //     if let Some(content) = parse_zipfile(&mut file) {
    //         insert_geojson(&mut import_pois, &content);
    //     }
    // }

    // import_pois
}

fn parse_zipfile(zip_file: &mut ZipFile) -> Option<String> {
    if !zip_file.is_file() {
        return None;
    }
    let file_path = zip_file.enclosed_name()?;
    let scraper = file_path.file_stem().unwrap().to_str().unwrap();
    if !SCRAPERS.contains(&scraper) {
        return None;
    }
    if INVALID_SCRAPERS.contains(&scraper) {
        return None;
    }
    debug!("reading {file_path:?}");
    stdout().flush().unwrap();
    let mut buffer = String::new();
    zip_file.read_to_string(&mut buffer).unwrap();
    if buffer.is_empty() {
        debug!("empty");
        return None;
    }
    Some(buffer)
}

fn insert_geojson(content: &str) -> impl Iterator<Item = ImportPoi> {
    let Ok(geojson) = content.parse::<GeoJson>() else {
        warn!("failed to parse geojson");
        panic!();
    };
    let feature_collection = FeatureCollection::try_from(geojson).unwrap();
    feature_collection
        .features
        .into_iter()
        .filter_map(|feature| {
            let Some(geojson::feature::Id::String(id)) = feature.id.clone() else {
                panic!("no id in feature");
            };
            let Some(geometry) = feature.geometry.clone() else {
                return None;
            };
            let geojson::Value::Point(point) = geometry.value else {
                return None;
            };

            let properties = feature
                .properties
                .clone()
                .unwrap()
                .into_iter()
                .filter_map(|(a, b)| json_value_to_string(&b).map(|v| (a, v)))
                .collect::<HashMap<String, String>>();

            // id's without ref aren't unique
            if !properties.contains_key("ref") {
                panic!("no ref in feature");
            }

            let (tags, metadata) = properties_to_tags_and_metadata(properties);
            Some(ImportPoi {
                id,
                lat: point[1],
                lon: point[0],
                tags,
                metadata,
            })
        })
}

fn properties_to_tags_and_metadata(
    mut properties: HashMap<String, String>,
) -> (HashMap<String, String>, HashMap<String, String>) {
    properties.remove("ref");
    properties.remove("nsi_id");
    let spider = properties.remove("@spider").unwrap();
    properties.entry("website".into()).and_modify(http_to_https);

    let mut metadata = HashMap::new();
    metadata.insert("spider".into(), spider);

    (properties, metadata)
}

fn http_to_https(url: &mut String) {
    if let Some(url_without_protocol) = url.strip_prefix("http://") {
        *url = format!("https://{url_without_protocol}");
    };
}

/// This function is required because simply calling `.to_string()` wraps output in quotes
fn json_value_to_string(value: &serde_json::Value) -> Option<String> {
    match value {
        serde_json::Value::Null
        | serde_json::Value::Bool(_)
        | serde_json::Value::Array(_)
        | serde_json::Value::Object(_) => None,
        serde_json::Value::Number(value) => Some(value.to_string()),
        serde_json::Value::String(value) => Some(value.clone()),
    }
}
