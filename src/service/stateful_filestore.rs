use std::{fs, path::PathBuf};

use crate::utils::get_project_dirs;

pub fn get_or_generate(key: &str, state: &str, generator: impl FnOnce(PathBuf)) -> PathBuf {
    let project_dirs = get_project_dirs();
    let filestore_path = project_dirs.cache_dir().join("filestore");

    let content_path = filestore_path.join("content").join(key);
    let state_path = filestore_path.join("state").join(key);

    if state_path.exists() {
        let prev_state = fs::read_to_string(&state_path).unwrap();
        if prev_state == state {
            return content_path;
        }
    };

    if content_path.exists() {
        if content_path.is_dir() {
            fs::remove_dir_all(&content_path).unwrap();
        } else {
            fs::remove_file(&content_path).unwrap();
        }
        fs::remove_file(&state_path).unwrap();
    }

    fs::create_dir_all(content_path.parent().unwrap()).unwrap();
    generator(content_path.clone());
    assert!(
        content_path.exists(),
        "filestore generator for key = {key:?} failed"
    );
    fs::create_dir_all(state_path.parent().unwrap()).unwrap();
    fs::write(&state_path, state).unwrap();
    content_path
}
