use std::{
    collections::{HashMap, HashSet},
    fs,
    io::Write,
    path::PathBuf,
    str::FromStr,
    sync::{Arc, Mutex},
    time::Instant,
};

use super::super::service;
use levenshtein::levenshtein;
use log::info;
use longitude::Location;
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};
use url::Url;

use crate::{
    model::{self, existing_poi::ExistingPoi, import_poi::ImportPoi},
    module::{alltheplaces::AllThePlaces, Module},
    service::osm,
    utils::{chunked, download_file},
};

pub async fn run() {
    tokio::task::spawn_blocking(run_sync).await.unwrap();
}

pub fn run_sync() {
    let module = AllThePlaces {};
    info!("loading module {}", module.get_name());

    for region in module.get_regions() {
        let existing_pois = HashSet::new();
        let existing_pois = Arc::new(Mutex::new(existing_pois));
        let extract_path = get_extract_path_for_region(&region);
        info!("importing region {}", region);
        osm::import(Arc::clone(&existing_pois), &extract_path, &get_tags());
        let existing_pois = existing_pois.lock().unwrap();
        info!("loaded existing pois of size = {}", existing_pois.len());

        let result_matched = vec![];
        let result_matched = Arc::new(Mutex::new(result_matched));

        let mut total_import_pois_count = 0;
        let start = Instant::now();
        for import_pois_chunk in chunked(module.get_import_pois(), 10_000) {
            total_import_pois_count += import_pois_chunk.len();
            info!(
                "loaded import pois chunk of size = {}",
                import_pois_chunk.len()
            );
            import_pois_chunk.par_iter().for_each(|import_poi| {
                for existing_poi in existing_pois.iter() {
                    if is_existing_poi_same_as_import_poi(existing_poi, import_poi) {
                        let entry = (
                            import_poi.metadata.get("spider").unwrap().to_owned(),
                            import_poi.id.clone(),
                            match existing_poi.r#type {
                                model::poi_type::PoiType::Node => "node",
                                model::poi_type::PoiType::Way => "way",
                                model::poi_type::PoiType::Relation => "relation",
                            }
                            .to_owned(),
                            existing_poi.id,
                        );
                        result_matched.lock().unwrap().push(entry);
                    }
                }
            });

            info!(
                "{} matches/s",
                total_import_pois_count as u64 / Instant::now().duration_since(start).as_secs()
            );
        }
        info!(
            "took {} seconds",
            Instant::now().duration_since(start).as_secs()
        );
        info!("import pois count = {}", total_import_pois_count);

        let result_matched = Arc::try_unwrap(result_matched).unwrap();
        let result_matched = result_matched.into_inner().unwrap();

        let mut import_poi_count_agg: HashMap<(&str, &str), usize> = HashMap::new();
        let mut existing_poi_count_agg: HashMap<(&str, i64), usize> = HashMap::new();
        for entry in &result_matched {
            *import_poi_count_agg
                .entry((&entry.0, &entry.1))
                .or_default() += 1;
            *existing_poi_count_agg
                .entry((&entry.2, entry.3))
                .or_default() += 1;
        }

        println!("duplicit import_poi matches");
        for (key, value) in import_poi_count_agg {
            if value > 1 {
                println!("{key:?}");
            }
        }

        println!("duplicit existing_poi matches");
        for (key, value) in existing_poi_count_agg {
            if value > 1 {
                println!("{key:?}");
            }
        }

        let path = format!("{}-output.csv", region.replace('/', "-"));
        let mut results_matched_file = fs::File::create(path).unwrap();
        for entry in result_matched {
            results_matched_file
                .write_all(format!("{},{},{}/{}\n", entry.0, entry.1, entry.2, entry.3).as_bytes())
                .unwrap();
        }
        results_matched_file.flush().unwrap();
    }
}

fn get_extract_path_for_region(region: &str) -> PathBuf {
    let key = format!("region-extract/{region}");
    let state_file_url = format!("https://download.openstreetmap.fr/extracts/{region}.osm.pbf.md5");
    let state = reqwest::blocking::get(&state_file_url)
        .unwrap()
        .text()
        .unwrap();
    let generator = |content_path| {
        info!("downloading region {}", region);
        let content_url = format!("https://download.openstreetmap.fr/extracts/{region}.osm.pbf");
        download_file(&Url::from_str(&content_url).unwrap(), &content_path);
    };
    service::stateful_filestore::get_or_generate(&key, &state, generator)
}

fn get_tags() -> HashSet<(String, String)> {
    let content = include_bytes!("../../tags.txt");
    let mut rdr = csv::Reader::from_reader(&content[..]);
    rdr.records()
        .map(|r| r.unwrap())
        .map(|r| (r.get(0).unwrap().to_owned(), r.get(1).unwrap().to_owned()))
        .collect()
}

fn is_existing_poi_same_as_import_poi(existing_poi: &ExistingPoi, import_poi: &ImportPoi) -> bool {
    let existing_poi_location = Location::from(existing_poi.lat, existing_poi.lon);
    let import_poi_location = Location::from(import_poi.lat, import_poi.lon);
    let distance = existing_poi_location
        .distance(&import_poi_location)
        .meters();
    if !distance.is_finite() || distance > 100.0 {
        return false;
    }

    let Some(existing_poi_name) = existing_poi.tags.get("name") else {
        return false;
    };
    let Some(import_poi_name) = import_poi.tags.get("name") else {
        return false;
    };

    let poi_name_difference = levenshtein(
        &existing_poi_name.to_lowercase(),
        &import_poi_name.to_lowercase(),
    );
    if poi_name_difference > 2 {
        return false;
    }

    let Some(existing_poi_category) = get_category_from_tags(&existing_poi.tags) else {
        return false;
    };
    let Some(import_poi_category) = get_category_from_tags(&import_poi.tags) else {
        return false;
    };
    if existing_poi_category != import_poi_category {
        return false;
    }

    true
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
enum Category {
    CarShop,
    Food,
    FitnessCentre,
    Fuel,
    Market,
    Pharmacy,
    FurnitureShop,
}

#[macro_export]
macro_rules! tagset {
    ( $( ($x:expr, $y:expr) ),* ) => {
        {
            let mut temp_hashset = HashSet::new();
            $(
                temp_hashset.insert(($x.to_owned(), $y.to_owned()));
            )*
            temp_hashset
        }
    };
}

fn get_category_from_tags(tags: &HashMap<String, String>) -> Option<Category> {
    let a = vec![
        (
            tagset!(
                ("shop", "car"),
                ("shop", "car_parts"),
                ("shop", "car_repair")
            ),
            Category::CarShop,
        ),
        (
            tagset!(
                ("amenity", "bar"),
                ("amenity", "cafe"),
                ("amenity", "restaurant"),
                ("amenity", "pub"),
                ("amenity", "fast_food")
            ),
            Category::Food,
        ),
        (
            tagset!(("leisure", "fitness_centre")),
            Category::FitnessCentre,
        ),
        (tagset!(("amenity", "fuel")), Category::Fuel),
        (
            tagset!(("amenity", "supermarket"), ("amenity", "convenience")),
            Category::Market,
        ),
        (
            tagset!(("amenity", "pharmacy"), ("shop", "chemist")),
            Category::Pharmacy,
        ),
        (tagset!(("shop", "furniture")), Category::FurnitureShop),
    ];

    for (tagset, category) in a {
        for (tagset_class, tagset_subclass) in tagset {
            if let Some(subclass) = tags.get(&tagset_class) {
                if subclass == &tagset_subclass {
                    return Some(category);
                }
            }
        }
    }

    None
}
