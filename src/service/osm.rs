use std::{
    collections::{BTreeMap, HashMap, HashSet},
    path::Path,
    sync::{Arc, Mutex, RwLock},
};

use log::debug;
use osmpbf::{Element, ElementReader, RelMember, RelMemberType, Way};

use crate::model::{
    existing_poi::ExistingPoi, inverted_index_b_tree_map::InvertedIndexBTreeMap, poi_type::PoiType,
};

pub type ExistingPoisWrapper = Arc<Mutex<HashSet<ExistingPoi>>>;

pub fn import(
    existing_pois: ExistingPoisWrapper,
    osm_file_path: impl AsRef<Path>,
    interesting_tags: &HashSet<(String, String)>,
) {
    debug!("importing file {:?}", &osm_file_path.as_ref());
    debug!("starting stage 1");
    let stage_1 = stage_1(&osm_file_path, interesting_tags);
    debug!("starting stage 2");
    let stage_2 = stage_2(&osm_file_path, interesting_tags, stage_1);
    debug!("starting stage 3");
    let stage_3 = stage_3(
        &osm_file_path,
        Arc::clone(&existing_pois),
        interesting_tags,
        stage_2,
    );
    debug!("starting stage 4");
    let stage_4 = stage_4(
        &osm_file_path,
        Arc::clone(&existing_pois),
        interesting_tags,
        stage_3,
    );
    debug!("starting stage 5");
    stage_5(&osm_file_path, existing_pois.clone(), stage_4);
    debug!("finished reading");
}

#[derive(Default)]
struct Stage1 {
    node_to_relations_map: Arc<RwLock<InvertedIndexBTreeMap<i64, i64>>>,
    way_to_relations_map: Arc<RwLock<InvertedIndexBTreeMap<i64, i64>>>,
}

/// Finds dependencies of **interesting relations** required to calculate their center point.
fn stage_1(
    osm_file_path: impl AsRef<Path>,
    interesting_tags: &HashSet<(String, String)>,
) -> Stage1 {
    let stage_1 = Stage1::default();

    read(osm_file_path, |element| {
        let Element::Relation(relation) = element else {
            return;
        };
        if !is_interesting_element_by_tags(interesting_tags, relation.tags()) {
            return;
        }

        let mut node_to_relations_map = stage_1.node_to_relations_map.write().unwrap();
        let mut way_to_relations_map = stage_1.way_to_relations_map.write().unwrap();
        for member in relation.members().filter(is_interesting_member) {
            match member.member_type {
                RelMemberType::Node => {
                    node_to_relations_map.push(member.member_id, relation.id());
                }
                RelMemberType::Way => {
                    way_to_relations_map.push(member.member_id, relation.id());
                }
                RelMemberType::Relation => {
                    unreachable!("Relation gets filtered out by is_interesting_member")
                }
            }
        }
    });

    stage_1
}

#[derive(Default)]
struct Stage2 {
    node_to_relations_map: Arc<RwLock<InvertedIndexBTreeMap<i64, i64>>>,
    way_to_relations_map: Arc<RwLock<InvertedIndexBTreeMap<i64, i64>>>,
    node_to_ways_map: Arc<RwLock<InvertedIndexBTreeMap<i64, i64>>>,
}

/// Finds dependencies of **interesting ways** and **ways that are dependencies of relations** required to calculate their center point.
fn stage_2(
    osm_file_path: impl AsRef<Path>,
    interesting_tags: &HashSet<(String, String)>,
    stage_1: Stage1,
) -> Stage2 {
    let stage_2 = Stage2 {
        node_to_relations_map: stage_1.node_to_relations_map,
        way_to_relations_map: stage_1.way_to_relations_map,
        ..Default::default()
    };

    read(osm_file_path, |element| {
        let Element::Way(way) = element else {
            return;
        };

        let way_to_relations_map = stage_2.way_to_relations_map.read().unwrap();
        if !is_interesting_way(interesting_tags, &way)
            && !way_to_relations_map.contains_key(&way.id())
        {
            return;
        }

        let mut node_to_ways_map = stage_2.node_to_ways_map.write().unwrap();
        // #skip_first_node_in_closed_way skipping first node of a closed way
        for node_id in way.refs().skip(1) {
            node_to_ways_map.push(node_id, way.id());
        }
    });

    stage_2
}

#[derive(Default)]
struct Stage3 {
    member_to_relations_position_sum_map: Arc<RwLock<BTreeMap<i64, (f64, f64)>>>,
    way_to_relations_map: Arc<RwLock<InvertedIndexBTreeMap<i64, i64>>>,
    node_to_ways_position_sum_map: Arc<RwLock<BTreeMap<i64, (f64, f64)>>>,
}

/// Imports **interesting nodes** and resolves node dependencies of **interesting relations**
fn stage_3(
    osm_file_path: impl AsRef<Path>,
    existing_pois: ExistingPoisWrapper,
    interesting_tags: &HashSet<(String, String)>,
    stage_2: Stage2,
) -> Stage3 {
    let stage_3 = Stage3 {
        way_to_relations_map: stage_2.way_to_relations_map,
        ..Default::default()
    };

    read(osm_file_path, |element| {
        let Element::DenseNode(dense_node) = element else {
            return;
        };

        if is_interesting_element_by_tags(interesting_tags, dense_node.tags()) {
            let tag_map = dense_node.tags().collect::<HashMap<&str, &str>>();
            existing_pois.lock().unwrap().insert(ExistingPoi {
                r#type: PoiType::Node,
                id: dense_node.id,
                lat: dense_node.lat(),
                lon: dense_node.lon(),
                tags: hashmap_str_str_to_owned(tag_map),
            });
        }

        {
            let node_to_relations_map = stage_2.node_to_relations_map.read().unwrap();
            if let Some(relations_ids) = node_to_relations_map.get_slice(&dense_node.id) {
                let mut member_to_relations_position_sum_map = stage_3
                    .member_to_relations_position_sum_map
                    .write()
                    .unwrap();
                for relation_id in relations_ids {
                    let (lat_sum, lon_sum) = member_to_relations_position_sum_map
                        .entry(*relation_id)
                        .or_default();
                    *lat_sum += dense_node.lat();
                    *lon_sum += dense_node.lon();
                }
            };
        };
        {
            let node_to_ways_map = stage_2.node_to_ways_map.read().unwrap();
            if let Some(ways_ids) = node_to_ways_map.get_slice(&dense_node.id) {
                let mut node_to_ways_position_sum_map =
                    stage_3.node_to_ways_position_sum_map.write().unwrap();
                for way_id in ways_ids {
                    let (lat_sum, lon_sum) =
                        node_to_ways_position_sum_map.entry(*way_id).or_default();
                    *lat_sum += dense_node.lat();
                    *lon_sum += dense_node.lon();
                }
            };
        };
    });

    stage_3
}

#[derive(Default)]
struct Stage4 {
    member_to_relations_position_sum_map: Arc<RwLock<BTreeMap<i64, (f64, f64)>>>,
}

/// Imports **interesting ways** and resolves ways dependencies of **interesting relations**
fn stage_4(
    osm_file_path: impl AsRef<Path>,
    existing_pois: ExistingPoisWrapper,
    interesting_tags: &HashSet<(String, String)>,
    stage_3: Stage3,
) -> Stage4 {
    let stage_4 = Stage4 {
        member_to_relations_position_sum_map: stage_3.member_to_relations_position_sum_map,
    };

    read(osm_file_path, |element| {
        let Element::Way(way) = element else {
            return;
        };

        let node_to_position_sum_map = stage_3.node_to_ways_position_sum_map.read().unwrap();
        let Some(position_sum) = node_to_position_sum_map.get(&way.id()) else {
            return;
        };

        // #skip_first_node_in_closed_way skipping first node of a closed way
        let node_count = way.refs().count() - 1;
        let lat = position_sum.0 / (node_count as f64);
        let lon = position_sum.1 / (node_count as f64);

        if is_interesting_way(interesting_tags, &way) {
            let tag_map = way.tags().collect::<HashMap<&str, &str>>();
            existing_pois.lock().unwrap().insert(ExistingPoi {
                r#type: PoiType::Way,
                id: way.id(),
                lat,
                lon,
                tags: hashmap_str_str_to_owned(tag_map),
            });
        }

        {
            let way_to_relations_map = stage_3.way_to_relations_map.read().unwrap();
            if let Some(relations_ids) = way_to_relations_map.get_slice(&way.id()) {
                let mut member_to_relations_position_sum_map = stage_4
                    .member_to_relations_position_sum_map
                    .write()
                    .unwrap();
                for relation_id in relations_ids {
                    let (lat_sum, lon_sum) = member_to_relations_position_sum_map
                        .entry(*relation_id)
                        .or_default();
                    *lat_sum += lat;
                    *lon_sum += lon;
                }
            };
        };
    });

    stage_4
}

/// Imports **interesting relations**
fn stage_5(osm_file_path: impl AsRef<Path>, existing_pois: ExistingPoisWrapper, stage_4: Stage4) {
    read(osm_file_path, |element| {
        let Element::Relation(relation) = element else {
            return;
        };

        let member_to_relations_position_sum_map =
            stage_4.member_to_relations_position_sum_map.read().unwrap();
        let Some(position_sum) = member_to_relations_position_sum_map.get(&relation.id()) else {
            return;
        };

        let node_count = relation.members().filter(is_interesting_member).count();
        let lat = position_sum.0 / (node_count as f64);
        let lon = position_sum.1 / (node_count as f64);

        let tag_map = relation.tags().collect::<HashMap<&str, &str>>();

        existing_pois.lock().unwrap().insert(ExistingPoi {
            r#type: PoiType::Relation,
            id: relation.id(),
            lat,
            lon,
            tags: hashmap_str_str_to_owned(tag_map),
        });
    });
}

fn is_interesting_element_by_tags<'a>(
    interesting_tags: &HashSet<(String, String)>,
    mut element_tags: impl Iterator<Item = (&'a str, &'a str)>,
) -> bool {
    element_tags.any(|(key, value)| interesting_tags.contains(&(key.to_owned(), value.to_owned())))
}

fn is_interesting_way(interesting_tags: &HashSet<(String, String)>, way: &Way) -> bool {
    is_interesting_element_by_tags(interesting_tags, way.tags()) && is_closed_way(way)
}

fn is_interesting_member(member: &RelMember) -> bool {
    member.member_type != RelMemberType::Relation && matches!(member.role(), Ok("outer"))
}

fn is_closed_way(way: &Way) -> bool {
    let Some(first_node_id) = way.refs().next() else {
        return false;
    };
    let last_node_id = way.refs().last().unwrap();
    first_node_id == last_node_id
}

fn hashmap_str_str_to_owned(hashmap: HashMap<&str, &str>) -> HashMap<String, String> {
    hashmap
        .into_iter()
        .map(|(k, v)| (k.to_owned(), v.to_owned()))
        .collect()
}

fn read<F: Fn(osmpbf::Element) + Send + Sync>(path: impl AsRef<Path>, callback: F) {
    let reader = ElementReader::from_path(&path).unwrap();
    reader
        .par_map_reduce(
            move |element| {
                callback(element);
            },
            || (),
            |(), ()| (),
        )
        .unwrap();
}
