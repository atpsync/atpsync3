use std::{
    fs, iter,
    path::{Path, PathBuf},
    process::Command,
};

use directories::ProjectDirs;
use log::info;
use rand::Rng;
use url::Url;

pub fn download_file(url: &Url, output: impl AsRef<Path>) {
    info!("downloading file from {url}");

    let temp_file = temp_file_in_cache();

    let mut command = Command::new("wget");
    command
        .arg("-qO")
        .arg(&temp_file)
        .arg("--show-progress")
        .arg(url.as_str());

    let mut child = command.spawn().unwrap();
    let exit_status = child.wait().unwrap();
    if !exit_status.success() {
        panic!("wget failed with exit status: {exit_status}");
    }

    fs::create_dir_all(output.as_ref().parent().unwrap()).unwrap();
    fs::rename(temp_file, output).unwrap();
}

#[must_use]
pub fn temp_file_in_cache() -> PathBuf {
    let project_dirs = get_project_dirs();
    let cache_dir = project_dirs.cache_dir();
    let temp_dir = cache_dir.join("temporary");
    fs::create_dir_all(&temp_dir).unwrap();
    let filename = generate_random_filename(32);
    temp_dir.join(filename)
}

#[must_use]
fn generate_random_filename(len: usize) -> String {
    const CHARSET: &[u8] = b"abcdefghijklmnopqrstuvwxyz0123456789";
    let mut rng = rand::thread_rng();
    let one_char = || CHARSET[rng.gen_range(0..CHARSET.len())] as char;
    iter::repeat_with(one_char).take(len).collect()
}

#[must_use]
pub fn get_project_dirs() -> ProjectDirs {
    ProjectDirs::from("cz", "vfosnar", "unisync").unwrap()
}

pub const TOP_LEVEL_TAGS: [&str; 29] = [
    "aerialway",
    "aeroway",
    "amenity",
    "barrier",
    "boundary",
    "building",
    "craft",
    "emergency",
    "geological",
    "healthcare",
    "highway",
    "historic",
    "landuse",
    "leisure",
    "man_made",
    "military",
    "natural",
    "office",
    "place",
    "power",
    "public_transport",
    "railway",
    "route",
    "shop",
    "sport",
    "telecom",
    "tourism",
    "water",
    "waterway",
];

pub fn chunked<I>(
    a: impl IntoIterator<Item = I>,
    chunk_size: usize,
) -> impl Iterator<Item = Vec<I>> {
    let mut a = a.into_iter();
    std::iter::from_fn(move || {
        Some(a.by_ref().take(chunk_size).collect()).filter(|chunk: &Vec<_>| !chunk.is_empty())
    })
}
