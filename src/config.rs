use clap::{command, Parser, Subcommand};

#[derive(Debug, Parser, Clone)]
#[command(version, about, long_about = None)]
pub struct Config {
    #[command(subcommand)]
    pub command: Command,
}

#[derive(Debug, Subcommand, Clone)]
pub enum Command {
    Run {},
}
