
CREATE TABLE results_matched
(
    module_id text   not null,
    import_id text   not null,
    osm_type  char   not null,
    osm_id    bigint not null,
    primary key (module_id, import_id)
);
CREATE TABLE results_ruled_out
(
    module_id text not null,
    import_id text not null,
    primary key (module_id, import_id)
);
CREATE TABLE results_missing
(
    module_id text not null,
    import_id text not null,
    primary key (module_id, import_id)
);

CREATE TABLE new_results_matched AS TABLE results_matched;
CREATE TABLE new_results_ruled_out AS TABLE results_ruled_out;
CREATE TABLE new_results_missing AS TABLE results_missing;

CREATE TABLE manual_create
(
    module_id text not null,
    import_id text not null,
    primary key (module_id, import_id),
    foreign key (module_id, import_id) references results_missing (module_id, import_id)
);

CREATE TABLE manual_update
(
    module_id text not null,
    import_id text not null,
    primary key (module_id, import_id),
    foreign key (module_id, import_id) references results_matched (module_id, import_id)
);

-- 0 = approval
-- can't contribute because:
-- 1 = already exists in OSM
-- 2 = doesn't actually exist
-- 3 = wrong match
-- 4 = wrong tags
CREATE TABLE manual_create_report
(
    module_id      text      not null,
    import_id      text      not null,
    contributor_id uuid      not null,
    timestamp      timestamp not null,
    type           int       not null,
    primary key (module_id, import_id),
    foreign key (module_id, import_id) references results_missing (module_id, import_id)
);
CREATE TABLE manual_update_report
(
    module_id      text      not null,
    import_id      text      not null,
    contributor_id uuid      not null,
    timestamp      timestamp not null,
    type           int       not null,
    primary key (module_id, import_id),
    foreign key (module_id, import_id) references results_missing (module_id, import_id)
);
